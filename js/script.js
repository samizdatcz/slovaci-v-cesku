var colors = ['#FF4136', '#0074D9', '#B10DC9', '#3D9970', '#FF851B', '#FFDC00','#001F3F', '#2ECC40']

var demo = ['0-14', '15-19', '20-29', '30-39', '40-49', '50-59', '60-69', '70-79', '80+'];

var uni = [
{name: "Janáčkova akademie múzických umění v Brně", y: 17.2, muzi: 44.7},
{name: "Masarykova univerzita", y: 16.9, muzi: 46.3},
{name: "Vysoké učení technické v Brně", y: 16.3, muzi: 73.5},
{name: "Veterinární a farmaceutická univerzita Brno", y: 14.4, muzi: 15.0},
{name: "Mendelova univerzita", y: 10.7, muzi: 38.0},
{name: "Vysoká škola ekonomická v Praze", y: 9.3, muzi: 42.0},
{name: "Vysoká škola umělecko-průmyslová v Praze", y: 8.7, muzi: 42.9},
{name: "Univerzita Karlova", y: 7.5, muzi: 39.8},
{name: "Univerzita Tomáše Bati ve Zlíně", y: 6.6, muzi: 45.4},
{name: "Vysoká škola chemicko-technologická v Praze", y: 6.5, muzi: 25.6}
]

var fak = [
{name:"Fakulta informatiky MU",y:49.2,muzi:83.2},
{name:"Fakulta informačních technologií VUT",y:31.8,muzi:91.9},
{name:"Lékařská fakulta MU",y:26.1,muzi:35.5},
{name:"Lékařská fakulta UP",y:25.3,muzi:28.4},
{name:"Ekonomicko-správní fakulta MU",y:22.8,muzi:54.1},
{name:"Fakulta architektury VUT",y:22.0,muzi:36.1},
{name:"Fakulta chemická VUT",y:21.0,muzi:33.2},
{name:"Farmaceutická fakulta UK v Hradci Králové",y:20.9,muzi:15.3},
{name:"Přírodovědecká fakulta MU",y:20.7,muzi:41.5},
{name:"Matematicko-fyzikální fakulta UK",y:20.3,muzi:72.8}
]

$(function () {

    Highcharts.setOptions({
            lang: {
                months: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                weekdays: ['neděle', 'pondělí', 'úterý', 'středa', 'čtvrtek', 'pátek', 'sobota'],
                shortMonths: ['leden', 'únor', 'březen', 'duben', 'květen', 'červen', 'červenec', 'srpen', 'září', 'říjen', 'listopad', 'prosinec'],
                thousandsSep: '',
                decimalPoint:',',
                rangeSelectorZoom: 'Zobrazit'
            }
        });

$(function () {
    $('#demosk').highcharts({
            chart: {
                type: 'bar'
            },
            title: {
                text: 'Slováci v Česku, věk a pohlaví'
            },
            subtitle: {
                text: 'podle státního občanství, rok 2011'
            },
            xAxis: [{
                categories: demo,
                reversed: false,
                labels: {
                    step: 1
                }
            }, {
                opposite: true,
                reversed: false,
                categories: demo,
                linkedTo: 0,
                labels: {
                    step: 1
                }
            }],
            yAxis: {
                title: {
                    text: null
                },
                labels: {
                    formatter: function () {
                        return Math.abs(this.value);
                    }
                }
            },
            exporting: {
                enabled: false
            },
            plotOptions: {
                series: {
                    stacking: 'normal'
                }
            },
            credits: {
                text : "Zdroj: ČSÚ"
            },
            tooltip: {
                formatter: function () {
                    if (this.series.name == 'Muži') {
                        return '<b>' + this.series.name + ', ' + this.point.category + ' let</b><br/>' + Highcharts.numberFormat(Math.abs(this.point.y), 0) + ' (' + -Math.round(this.y/81984*1000)/10 + ' % slovenské menšiny v Česku)'
                    } else {
                        return '<b>' + this.series.name + ', ' + this.point.category + ' let</b><br/>' + Highcharts.numberFormat(Math.abs(this.point.y), 0) + ' (' + Math.round(this.y/81984*1000)/10 + ' % slovenské menšiny v Česku)'
                    }
                }
            },
            series: [{
                name: 'Muži',
                data: [-2815, -1169, -13218, -14819, -6705, -3919, -1008, -284, -105],
                color: colors[1]
            }, {
                name: 'Ženy',
                data: [2624, 1160, 14663, 12183, 3610, 2252, 695, 432, 323],
                color: colors[0]
            }]
    });
})

$(function () {
    $('#democr').highcharts({
            chart: {
                type: 'bar'
            },
            title: {
                text: 'Češi na Slovensku, věk a pohlaví'
            },
            subtitle: {
                text: 'podle státního občanství, rok 2011'
            },
            xAxis: [{
                categories: demo,
                reversed: false,
                labels: {
                    step: 1
                }
            }, {
                opposite: true,
                reversed: false,
                categories: demo,
                linkedTo: 0,
                labels: {
                    step: 1
                }
            }],
            yAxis: {
                title: {
                    text: null
                },
                labels: {
                    formatter: function () {
                        return Math.abs(this.value);
                    }
                }
            },
            exporting: {
                enabled: false
            },
            plotOptions: {
                series: {
                    stacking: 'normal'
                }
            },
            credits: {
                text : "Zdroj: ŠÚSR"
            },
            tooltip: {
                formatter: function () {
                    if (this.series.name == 'Muži') {
                        return '<b>' + this.series.name + ', ' + this.point.category + ' let</b><br/>' + Highcharts.numberFormat(Math.abs(this.point.y), 0) + ' (' + -Math.round(this.y/7177*1000)/10 + ' % české menšiny na Slovensku)'
                    } else {
                        return '<b>' + this.series.name + ', ' + this.point.category + ' let</b><br/>' + Highcharts.numberFormat(Math.abs(this.point.y), 0) + ' (' + Math.round(this.y/7177*1000)/10 + ' % české menšiny na Slovensku)'
                    }
                }
            },
            series: [{
                name: 'Muži',
                data: [-312, -91, -347, -653, -610, -609, -525, -283, -86],
                color: colors[1]
            }, {
                name: 'Ženy',
                data: [309, 127, 547, 684, 516, 429, 568, 324, 157],
                color: colors[0]
            }]
    });
});

$(function () {
    $('#mensiny').highcharts({
        chart: {
            type: 'line'
        },
        title: {
            text: 'Menšiny v Česku'
        },
        subtitle: {
            text: 'podle státního občanství'
        },
        xAxis: {
            title: {
                text: ''
            },
            categories: [2001, 2004, 2008, 2012, 2016]
        },
        yAxis: {
            title: {
                text: '',
            },
            labels: {
                format: '{value}'
            }
        },
        tooltip: {
            formatter: function() {
                if (this.x == 2001) {
                    return this.series.name + ': ' + this.y + ' (' + Math.round(this.y/10224*10)/100 + ' % obyvatel)';
                } else if (this.x == 2004) {
                    return this.series.name + ': ' + this.y + ' (' + Math.round(this.y/10207*10)/100 + ' % obyvatel)';
                } else if (this.x == 2008) {
                    return this.series.name + ': ' + this.y + ' (' + Math.round(this.y/10430*10)/100 + ' % obyvatel)';
                } else if (this.x == 2012) {
                    return this.series.name + ': ' + this.y + ' (' + Math.round(this.y/10509*10)/100 + ' % obyvatel)';
                } else if (this.x == 2016) {
                    return this.series.name + ': ' + this.y + ' (' + Math.round(this.y/10578*10)/100 + ' % obyvatel)';
                }

            }
        },
        exporting: {
            enabled: false
        },
        credits: {
            href : 'https://vdb.czso.cz/vdbvo2/faces/index.jsf?page=statistiky#katalog=31032',
            text : 'Zdroj: ČSÚ'
        },
        plotOptions: {
            line: {
                marker: {
                    symbol: "diamond"
                }
            }
        },
        series: [{
            name: 'Ukrajinci',
            data: [20628, 78263, 131921, 112549, 109850],
            color: colors[0],
        }, {
            name: 'Slováci',
            data: [24201, 47354, 76034, 85807, 107251],
            color: colors[1],
            lineWidth: 5
        }, {
            name: 'Vietnamci',
            data: [18210, 34179, 60255, 57300, 58025],
            color: colors[2],
        }, {
            name: 'Rusové',
            data: [7696, 14747, 27086, 32961, 35759],
            color: colors[3],
        }, {
            name: 'Němci',
            data: [3438, 5772, 17496, 17149, 21216],
            color: colors[4],
        }, {
            name: 'Poláci',
            data: [13350, 16265, 21710, 19235, 20305],
            color: colors[5],
        }]
    });
})

$(function () {
    $('#univerzity').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Univerzity s nejvíce Slováky'
        },
        subtitle: {
            text: 'podle státního občanství, rok 2016'
        },
        xAxis: {
            categories: uni.map(function(value, index) { return value['name']; })
        },
        yAxis: {
            labels: {
                format: '{value} %'
            },
            title: {
                text: 'podíl slovenských studentů',
            },
            reversedStacks: false
        },
        tooltip: {
            formatter: function() {
                return '<b>' + this.y + ' % slovenských studentů</b>, z toho ' + this.point.muzi + ' % mužů.'
            },
            crosshairs: true
        },
        exporting: {
            enabled: false
        },
        credits: {
            href : 'http://krakatau.uiv.cz/statistikyvs/',
            text : 'Zdroj: MŠMT'
        },
        plotOptions: {
        },
        series: [{
            name: 'univerzity',
            data: uni,
            color: colors[1],
            showInLegend: false
        }]
    });
})

$(function () {
    $('#fakulty').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: 'Fakulty s nejvíce Slováky'
        },
        subtitle: {
            text: 'podle státního občanství, rok 2016'
        },
        xAxis: {
            categories: fak.map(function(value, index) { return value['name']; })
        },
        yAxis: {
            labels: {
                format: '{value} %'
            },
            title: {
                text: 'podíl slovenských studentů',
            },
            reversedStacks: false
        },
        tooltip: {
            formatter: function() {
                return '<b>' + this.y + ' % slovenských studentů</b>, z toho ' + this.point.muzi + ' % mužů.'
            },
            crosshairs: true
        },
        exporting: {
            enabled: false
        },
        credits: {
            href : 'http://krakatau.uiv.cz/statistikyvs/',
            text : 'Zdroj: MŠMT'
        },
        plotOptions: {
        },
        series: [{
            name: 'fakulty',
            data: fak,
            color: colors[0],
            showInLegend: false
        }]
    });
})

});