title: "Slováci v Česku: mladší a vzdělanější než Češi"
perex: "Data o slovenské menšině: kde žijí, čím se živí, co studují a kolik jich vlastně na českých školách je?"
authors: ["Jan Boček"]
published: "17. července 2017"
coverimg: https://samizdat.cz/data/slovaci-v-cesku/media/sp.png
coverimg_note: ""
styles: []
libraries: ["https://unpkg.com/jquery@3.2.1", "https://code.highcharts.com/highcharts.js"]
options: "" #wide
---

Necelých deset let po rozdělení Československa to vypadalo, že Češi a Slováci se rozešli nejen papírově, ale i fakticky. Na přelomu století žilo v Česku pouhých 24 tisíc Slováků a na Slovensku ani ne sedm tisíc Čechů. Slovenskou menšinu navíc početně rychle přerostla ukrajinská a málem i vietnamská.

Od té doby ovšem Slováků vytrvale přibývá. Loni jich v Česku žilo 107 tisíc a pokud se trend nezmění, nejpozději za rok budou nejsilnější tuzemskou menšinou.

<wide><div id="mensiny" style="width:100%; height:600px"></div></wide>

*Graf ukazuje cizince, kteří mají v Česku trvalé nebo obvyklé bydliště, zároveň ale mají státní příslušnost jiného státu. Přihlásit se k české nebo slovenské státní příslušnosti sice československé zákony umožnily už v roce 1968, do rozdělení federace v roce 1993 ale měly minimální využití.*

*Historii česko-slovenských vztahů lépe ilustruje národnostní nebo jazyková menšina – občané Československa, kteří uvádějí slovenskou národnost nebo mluví slovensky. Česká a slovenská národnost se sleduje od založení Československa, v [grafickém kvízu](https://www.irozhlas.cz/veda-technologie/historie/kviz-vite-kdo-vydelal-na-rozdeleni-ceskoslovenska_1705300850_dp#content) proto ukazujeme právě národnost. Ze stejného důvodu ovšem data o národnosti příliš nevypovídají o současném vztahu obou států – řada zejména starších lidí na českém území se hlásí ke slovenské národnosti navzdory tomu, že větší část života strávili v západní částí federace, v roce 1993 si zvolili českou státní příslušnost a je tedy přirozenější považovat je za Čechy.*

V článku proto používáme data o státní příslušnosti. Pro ilustraci: při posledním českém sčítání obyvatel v roce 2011 zatrhlo

* 82 tisíc lidí (0,8 % obyvatel Česka) slovenské státní občanství,
* 147 tisíc lidí (1,4 %) slovenskou národnost,
* 154 tisíc lidí (1,5 %) slovenský mateřský jazyk a
* 290 tisíc lidí (2,8 %) místo narození na Slovensku.

Jsou mezi nimi i ti, kteří v Česku žijí pouze přechodně, například studenti. Poslední sčítání obyvatel se totiž týkalo všech lidí s aktuálním pobytem na území Česka ([definice ČSÚ](https://www.czso.cz/csu/sldb/nejcastejsi-dotazy?p_p_id=sucrrisfaqweb_WAR_faqportlet&p_p_lifecycle=0&p_p_state=normal&p_p_mode=view&p_p_col_id=column-2&p_p_col_count=3&p_p_col_pos=1&_sucrrisfaqweb_WAR_faqportlet__facesViewIdRender=%2Fviews%2FDetail.xhtml&_sucrrisfaqweb_WAR_faqportlet_faqId=112)); dřívější sčítání počítala s trvalým pobytem.

## Mladí muži s mobilními telefony

Slováci jsou oproti většinovým Čechům mladší (průměrně 33,1 vs. 41 let) a je mezi nimi více mužů (53,7 % vs. českých 49,1 %). Jsou také častěji svobodní (52 % vs. 40 %) a mají vyšší vzdělání (19,7 % vs. 12,5 % vysokoškoláků). Podle [analýzy ČSÚ](https://www.czso.cz/documents/10180/20540445/170219-14.pdf/1eae07fa-58cc-4ea0-a0b3-4646c1d8a8dc?version=1.0) data „jednoznačně potvrzují skutečnost, že imigrace občanů Slovenska má pracovní nebo studijní důvody“.

Následující grafy srovnávají slovenskou menšinu v Česku s českou menšinou na Slovensku. Kromě zmíněných demografických rozdílů říkají ještě to, že v roce 2011, kdy v obou zemích proběhlo sčítání obyvatel, byla česká diaspora na Slovensku desetkrát menší.

<wide><div id="demosk" style="width:100%; height:400px"></div></wide>
<wide><div id="democr" style="width:100%; height:400px"></div></wide>

## Za prací do zdravotnictví a továren na autodíly

Data ze sčítání obyvatel ukazují, kde Slováci žijí. Velikost kruhu odpovídá jejich počtu, sytost barvy podílu v obci.

<wide><img src="https://samizdat.cz/data/slovaci-v-cesku/media/sp.png" width="100%"></wide>

# Slovenská národnost
x